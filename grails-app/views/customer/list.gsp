<%@ page import="salao.Customer" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'customer.label', default: 'Customer')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <g:set var="customers" value="${customerInstanceList}"/>
        <table class="table table-striped table-hover table-bordered">
            <caption>Customers</caption>
            <thead>
                <th>#</th>
                <th>Name</th>
            </thead>
            <tbody>
                <g:each in="${customers}">
                    <tr>
                        <td>${it.id}</td>
                        <td>${it.name}</td>
                    </tr>
                </g:each>
            </tbody>
        </table>
    </body>
</html>
