<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript" src="/salao/js/jquery-1.11.0.js"></script>
        <script type="text/javascript" src="/salao/js/angular.js"></script>

        <link type="text/css" href="${resource(dir: 'css', file: 'bootstrap.css')}" rel="stylesheet" />
        <link type="text/css" href="${resource(dir: 'css', file: 'bootstrap-responsive.css')}" rel="stylesheet" />
        <title><g:layoutTitle default="Grails" /></title>
        <link rel="stylesheet" href="${resource(dir:'css',file:'main.css')}" />
        <link rel="shortcut icon" href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-icon" />
        <g:layoutHead />
        <g:javascript library="application" />
    </head>
    <body>
        <div id="spinner" class="spinner" style="display:none;">
            <img src="${resource(dir:'images',file:'spinner.gif')}" alt="${message(code:'spinner.alt',default:'Loading...')}" />
        </div>
        <div id="grailsLogo"><a href="http://grails.org"><img src="${resource(dir:'images',file:'grails_logo.png')}" alt="Grails" border="0" /></a></div>
        <g:layoutBody />
    </body>
</html>