<%@ page import="salao.Treatment" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'treatment.label', default: 'Treatment')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
        <script type="text/javascript">
        $(document).ready(function() {
            $('#addProduct').click(function() {
                var prodId = $('#products option:selected').attr('value')
                var description = $('#products option:selected').attr('description')
                $('#ulProducts').append("<li value=" + prodId + ">" + description + "</li>")    

                insertProducts()
            });

            function insertProducts() {
                var products = $('#ulProducts').find('li')
                var prodIds = []
                products.each(function() {
                    prodIds.push($(this).attr('value'))
                })
                $('#trtmntProducts').val(prodIds)
            }
        });
        </script>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${treatmentInstance}">
            <div class="errors">
                <g:renderErrors bean="${treatmentInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save">
                <div class="dialog">
                    <table>
                        <tbody>
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="customer"><g:message code="treatment.customer.label" default="Customer" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: treatmentInstance, field: 'customer', 'errors')}">
                                    <g:select name="customer.id" from="${salao.Customer.list()}" optionKey="id" value="${treatmentInstance?.customer?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="employee"><g:message code="treatment.employee.label" default="Employee" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: treatmentInstance, field: 'employee', 'errors')}">
                                    <g:select name="employee.id" from="${salao.Employee.list()}" optionKey="id" value="${treatmentInstance?.employee?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="price"><g:message code="treatment.price.label" default="Price" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: treatmentInstance, field: 'price', 'errors')}">
                                    <g:textField name="price" value="${fieldValue(bean: treatmentInstance, field: 'price')}" />
                                </td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="product">Products</label>
                                    <select id="products">
                                        <g:each in="${salao.Product.list()}" var="product">
                                            <option value="${product.id}" description="${product.description}">${product.description}</option>
                                        </g:each>
                                    </select>
                                    <span>
                                        <button type="button" id="addProduct" onclick="return false;">Add</button>
                                    </span>
                                    <input id="trtmntProducts" name="prodIds" type="hidden" value=""/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div>
                                        <ul id="ulProducts">
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><button class="save" onclick="return insertProducts()">Create</button></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
