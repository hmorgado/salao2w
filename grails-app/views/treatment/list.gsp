<%@ page import="salao.Treatment" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'treatment.label', default: 'Treatment')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><strong>Tratamentos</strong></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'treatment.id.label', default: 'Id')}" />
                        
                            <th><g:message code="treatment.customer.label" default="Customer" /></th>
                        
                            <th><g:message code="treatment.employee.label" default="Employee" /></th>
                        
                            <g:sortableColumn property="price" title="${message(code: 'treatment.price.label', default: 'Price')}" />
                        
                            <th><g:message code="treatment.products.label" default="Products" /></th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${treatmentInstanceList}" status="i" var="treatment">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${treatment.id}">${fieldValue(bean: treatment, field: "id")}</g:link></td>
                        
                            <td>${treatment.customer.name}</td>
                        
                            <td>${treatment.employee.name}</td>
                        
                            <td>${treatment.price}</td>
                        
                            <td>${treatment.products*.description}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${treatmentInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
