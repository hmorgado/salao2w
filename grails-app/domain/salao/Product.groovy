package salao

class Product {

	String description
	Treatment treatments

	static belongsTo = Treatment
	static hasMany = [treatments: Treatment]
	
    static constraints = {
    }
}
