package salao

class Treatment {

	Float price
	Customer customer
	Employee employee
	List<Product> products

	static hasMany = [products: Product]

    static constraints = {
    	customer nullable: true, blank: true
    	employee nullable: true, blank: true
    	products nullable: true, blank: true
    	price nullable: true, blank: true
    }
}
