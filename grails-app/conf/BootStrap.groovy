import salao.*

class BootStrap {

    def init = { servletContext ->

    	new Product(description: 'groovy').save(flush: true)
    	new Product(description: 'grails').save(flush: true)
    	new Product(description: 'crime').save(flush: true)
    	new Product(description: 'capeta').save(flush: true)
    	new Product(description: 'die in 2 days').save(flush: true)

    	new Customer(name: 'capeta').save(flush:true)
    	new Customer(name: 'eu').save(flush:true)
    	new Customer(name: 'voce').save(flush:true)
    	new Customer(name: 'sua mae').save(flush:true)

    	new Employee(name: 'vitor').save(flush:true)
    	new Employee(name: 'guilherme').save(flush:true)
    	new Employee(name: 'coisa').save(flush:true)
    	new Employee(name: 'frank').save(flush:true)
    	new Employee(name: 'MEEEEXXX').save(flush:true)
    	new Employee(name: 'mano alex').save(flush:true)



    }
    def destroy = {
    }
}
