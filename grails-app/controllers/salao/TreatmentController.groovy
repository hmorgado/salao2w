package salao

class TreatmentController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [treatmentInstanceList: Treatment.list(params), treatmentInstanceTotal: Treatment.count()]
    }

    def create = {
        def treatmentInstance = new Treatment()
        treatmentInstance.properties = params
        return [treatmentInstance: treatmentInstance]
    }

    def save = {
        def treatmentInstance = new Treatment(params)
        treatmentInstance.products = getProdList(params.prodIds)

        if (treatmentInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: 'treatment.label', default: 'Treatment'), treatmentInstance.id])}"
            redirect(action: "show", id: treatmentInstance.id)
        }
        else {
            render(view: "create", model: [treatmentInstance: treatmentInstance])
        }
    }

    private getProdList(prodIds){
        def ids = prodIds.split(',')
        def products = []
        ids.each{
            products << Product.get(it as Long)
        }
        products
    }

    def show = {
        def treatmentInstance = Treatment.get(params.id)
        def employee = Employee.get(treatmentInstance.employee.id)
        def customer = Customer.get(treatmentInstance.customer.id)

        if (!treatmentInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'treatment.label', default: 'Treatment'), params.id])}"
            redirect(action: "list")
        }
        else {
            [treatmentInstance: treatmentInstance, employee: employee, customer: customer, products: treatmentInstance.products]
        }
    }

    def edit = {
        def treatmentInstance = Treatment.get(params.id)
        if (!treatmentInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'treatment.label', default: 'Treatment'), params.id])}"
            redirect(action: "list")
        }
        else {
            return [treatmentInstance: treatmentInstance]
        }
    }

    def update = {
        def treatmentInstance = Treatment.get(params.id)
        if (treatmentInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (treatmentInstance.version > version) {
                    
                    treatmentInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'treatment.label', default: 'Treatment')] as Object[], "Another user has updated this Treatment while you were editing")
                    render(view: "edit", model: [treatmentInstance: treatmentInstance])
                    return
                }
            }
            treatmentInstance.properties = params
            if (!treatmentInstance.hasErrors() && treatmentInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'treatment.label', default: 'Treatment'), treatmentInstance.id])}"
                redirect(action: "show", id: treatmentInstance.id)
            }
            else {
                render(view: "edit", model: [treatmentInstance: treatmentInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'treatment.label', default: 'Treatment'), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        def treatmentInstance = Treatment.get(params.id)
        if (treatmentInstance) {
            try {
                treatmentInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'treatment.label', default: 'Treatment'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'treatment.label', default: 'Treatment'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'treatment.label', default: 'Treatment'), params.id])}"
            redirect(action: "list")
        }
    }
}
